import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LoginComponent } from './components/login/login.component';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {InterceptService} from './interceptors/intercept.service';
import {HashLocationStrategy, LocationStrategy} from '@angular/common';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {MatButtonModule} from '@angular/material/button';
import {ExtendedModule, FlexModule} from '@angular/flex-layout';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { MatIconModule } from '@angular/material/icon';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatListModule } from '@angular/material/list';
import { UsersComponent } from './components/users/users.component';
import { MerchantsComponent } from './components/merchants/merchants.component';
import { RequestsComponent } from './components/requests/requests.component';
import { MatTableModule } from '@angular/material/table';
import { MatCardModule } from '@angular/material/card';
import { MerchantDataComponent } from './components/merchant-data/merchant-data.component';

import {NgxMaterialTimepickerModule} from 'ngx-material-timepicker';
import { MatPaginatorIntl, MatPaginatorModule } from "@angular/material/paginator";
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatSliderModule } from '@angular/material/slider';
import { AddPolicyDialogComponent } from './components/dialogs/add-policy-dialog/add-policy-dialog.component';
import { MatDialogModule } from '@angular/material/dialog';
import { DeletePolicyDialogComponent } from './components/dialogs/delete-policy-dialog/delete-policy-dialog.component';
import { LogoutDialogComponent } from './components/dialogs/logout-dialog/logout-dialog.component';
import { MatProgressSpinnerModule } from "@angular/material/progress-spinner";
import { MainComponent } from './components/dashboard/main/main.component';
import { HeaderComponent } from './components/dashboard/shared/header/header.component';
import { InvoicesComponent } from './components/dashboard/invoices/invoices.component';
import { ExtremeTableComponent } from './components/shared/extreme-table/extreme-table.component';
import {
    DxChartModule,
    DxContextMenuModule,
    DxDataGridModule, DxHtmlEditorModule,
    DxPieChartModule,
    DxSelectBoxModule
} from "devextreme-angular";
import { CreateInvoiceDialogComponent } from './components/dashboard/invoices/create-invoice-dialog/create-invoice-dialog.component';
import { LoadingComponent } from './components/shared/loading/loading.component';
import { StoresComponent } from './components/dashboard/stores/stores.component';
import { ChartBarComponent } from './components/shared/chart-bar/chart-bar.component';
import { GraphPieComponent } from './components/shared/graph-pie/graph-pie.component';
import { ChartLineComponent } from './components/shared/chart-line/chart-line.component';
import { ChartContainerComponent } from './components/shared/chart-container/chart-container.component';
import { UnregisteredStoresComponent } from './components/dashboard/unregistered-stores/unregistered-stores.component';
import { OrdersComponent } from './components/dashboard/orders/orders.component';
import { PaymentsComponent } from './components/dashboard/payments/payments.component';
import { InvoiceInfoComponent } from './components/dashboard/invoices/invoice-info/invoice-info.component';
import {MatTabsModule} from "@angular/material/tabs";
import { PushContentDialogComponent } from './components/shared/push-content-dialog/push-content-dialog.component';
import { EmailCreatorDialogComponent } from './components/shared/email-creator-dialog/email-creator-dialog.component';
import { ExtremeEditorComponent } from './components/shared/extreme-editor/extreme-editor.component';
import {MatTooltipModule} from "@angular/material/tooltip";
import { EmailsComponent } from './components/dashboard/emails/emails.component';
import { NotificationsComponent } from './components/dashboard/notifications/notifications.component';
import { RedeemRequestsComponent } from './components/dashboard/redeem-requests/redeem-requests.component';


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    DashboardComponent,
    UsersComponent,
    MerchantsComponent,
    RequestsComponent,
    MerchantDataComponent,
    AddPolicyDialogComponent,
    DeletePolicyDialogComponent,
    LogoutDialogComponent,
    MainComponent,
    HeaderComponent,
    InvoicesComponent,
    ExtremeTableComponent,
    CreateInvoiceDialogComponent,
    LoadingComponent,
    StoresComponent,
    ChartBarComponent,
    GraphPieComponent,
    ChartLineComponent,
    ChartContainerComponent,
    UnregisteredStoresComponent,
    OrdersComponent,
    PaymentsComponent,
    InvoiceInfoComponent,
    PushContentDialogComponent,
    EmailCreatorDialogComponent,
    ExtremeEditorComponent,
    EmailsComponent,
    NotificationsComponent,
    RedeemRequestsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatFormFieldModule,
    MatInputModule,
    ReactiveFormsModule,
    HttpClientModule,
    MatButtonModule,
    FlexModule,
    ExtendedModule,
    MatSnackBarModule,
    MatIconModule,
    MatToolbarModule,
    MatSidenavModule,
    MatListModule,
    MatTableModule,
    MatCardModule,
    NgxMaterialTimepickerModule,
    MatPaginatorModule,
    MatCheckboxModule,
    FormsModule,
    MatSliderModule,
    MatDialogModule,
    MatProgressSpinnerModule,
    DxDataGridModule,
    DxSelectBoxModule,
    DxChartModule,
    DxPieChartModule,
    DxContextMenuModule,
    MatTabsModule,
    DxHtmlEditorModule,
    MatTooltipModule
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS, useClass: InterceptService, multi: true
    },
    {
      provide: LocationStrategy, useClass: HashLocationStrategy
    },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
