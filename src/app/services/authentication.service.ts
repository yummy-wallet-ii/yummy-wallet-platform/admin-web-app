import { Injectable } from '@angular/core';
import {UserLoginModel} from '../models/Business';
import {CommonService} from './common.service';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  constructor(private commonService: CommonService) {
  }

  /**
   * Logs in only admins
   * @param loginData
   */
  login = async (loginData: UserLoginModel): Promise<any> => {
    try {
      return await this.commonService.postRequest('users/login/admin', loginData);
    } catch (e) {
      throw e;
    }
  }

}
