import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import * as _ from 'lodash';
import * as moment from 'moment';

@Injectable({
  providedIn: 'root'
})
export class CommonService {

  constructor(
    private httpClient: HttpClient
  ) {
  }

  groupBy = (array: any, key: string) => {
    return _.groupBy(array, key);
  }
  find = (array: any[], key: string, value: any) => {
    return array.find(record => record[key] === value);
  }

  formatDate = (dt: string) => {
    try {
      const tempDate = dt.split(' ');
      const dateOnly = tempDate[0].split('-');
      const year = dateOnly[2];
      const month = dateOnly[1];
      const day = dateOnly[0];
      const stringDate = `${year}-${month}-${day} ${tempDate[1]}`;
      const date = new Date(stringDate);
      return moment(date).format('MM-DD-YYYY HH:mm');
    } catch (e) {
      return null;
    }
  }

  getRequest = async (endpoint: string): Promise<any> => {
    return new Promise((resolve, reject) => {
      this.httpClient.get(endpoint).subscribe(result => {
        resolve(result);
      }, error => reject(error));
    });
  }

  postRequest = async (endpoint: string, body: any): Promise<any> => {
    return new Promise((resolve, reject) => {
      this.httpClient.post(endpoint, body).subscribe(result => {
        resolve(result);
      }, error => reject(error));
    });
  }

  putRequest = async (endpoint: string, body: any): Promise<any> => {
    return new Promise((resolve, reject) => {
      this.httpClient.put(endpoint, body).subscribe(result => {
        resolve(result);
      }, error => reject(error));
    });
  }

  deleteRequest = async (endpoint: string): Promise<any> => {
    return new Promise((resolve, reject) => {
      this.httpClient.delete(endpoint).subscribe(result => {
        resolve(result);
      }, error => reject(error));
    });
  }

  public base64ToBlob(b64Data: any, contentType = '', sliceSize = 512): any {
    b64Data = b64Data.replace(/\s/g, '');
    const byteCharacters = atob(b64Data);
    const byteArrays = [];
    for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
      const slice = byteCharacters.slice(offset, offset + sliceSize);
      const byteNumbers = new Array(slice.length);
      for (let i = 0; i < slice.length; i++) {
        byteNumbers[i] = slice.charCodeAt(i);
      }
      const byteArray = new Uint8Array(byteNumbers);
      byteArrays.push(byteArray);
    }
    return new Blob(byteArrays, {type: contentType});
  }
}
