import {Injectable} from '@angular/core';
import {MatSnackBar} from '@angular/material/snack-bar';

@Injectable({
  providedIn: 'root'
})
export class ToastMessagesService {

  constructor(public snackBar: MatSnackBar) {
  }

  toastMessages(message: string, action?: any): void {
    action = action ? action : '';
    this.snackBar.open(message, action, {
      duration: 3000,
      panelClass: ['blue-snackbar']
    });
  }

  toastErrorMessage(message: string): void {
    this.snackBar.open(`Κάτι πήγε στραβά: ${message}`, '', {
      duration: 5000,
      panelClass: ['warning-snackbar']
    });
  }
}
