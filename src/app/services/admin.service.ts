import {Injectable} from '@angular/core';
import {CommonService} from './common.service';
import {Customer} from '../models/Customer';

@Injectable({
  providedIn: 'root'
})
export class AdminService {

  constructor(private commonService: CommonService) {
  }

  /**
   * Fetches users
   */
  getUsers = async (): Promise<Customer[]> => {
    try {
      return await this.commonService.getRequest('customers');
    } catch (e) {
      throw e;
    }
  }

  downloadInvoice = async (invoiceId: number) => {
    return await this.commonService.getRequest(`invoices/export/${invoiceId}`);
  }

  fetchEmailLogs = async (merchantId?: number): Promise<any> => {
    const endpoint = merchantId ? `logs/email?storeId=${merchantId}` : 'logs/email';
    return await this.commonService.getRequest(endpoint);
  }
  fetchNotificationLogs = async (merchantId?: number) => {
    const endpoint = merchantId ? `logs/notifications?storeId=${merchantId}` : 'logs/notifications';
    return await this.commonService.getRequest(endpoint);
  }

  fetchRedeemRequests = async () => {
    return await this.commonService.getRequest('redeem');
  }

  updateRedeemRequest = async (id: number, request: any) => {
    return await this.commonService.putRequest(`redeem/${id}`, {request});
  }

  sendEmail = async (body: any): Promise<any> => {
    return await this.commonService.postRequest('messages/email', body);
  }

  sendNotification = async (userTokens: string[], notification: any, storeId: any): Promise<void> => {
    const postBody = {
      recipients: userTokens,
      notification,
      storeId
    };

    return await this.commonService.postRequest('messages', postBody);
  }

  getInvoiceById = async (invoiceId: number) => {
    return await this.commonService.getRequest(`invoices/${invoiceId}`);
  }

  registerStore = async (storeId: number) => {
    return await this.commonService.putRequest('stores/register', {storeId});
  }

  updateStatusActivity = async (storeId: number, status: number) => {
    return await this.commonService.putRequest('stores/activation', {storeId, status});
  }

  getPayments = async (): Promise<any> => {
    try {
      return await this.commonService.getRequest('bucket');
    } catch (e) {
      throw e;
    }
  }

  getOrders = async (): Promise<any> => {
    try {
      return await this.commonService.getRequest('orders');
    } catch (e) {
      throw e;
    }
  }

  /**
   * Fetches stores
   */
  getMerchants = async (): Promise<any> => {
    try {
      return await this.commonService.getRequest('stores/registered');
    } catch (e) {
      throw e;
    }
  }

  /**
   * Fetches details of a store
   * @param id
   */
  getMerchantById = async (id: any): Promise<any> => {
    try {
      const store = await this.commonService.getRequest('stores/' + id);
      const storeOptions = await this.commonService.getRequest('stores/options/' + id);
      const result = new Array();
      result.push(store);
      result.push(storeOptions);
      return result;
    } catch (e) {
      throw e;
    }
  }

  getOrdersByMerchantId = async (id: number): Promise<any> => {
    return await this.commonService.getRequest(`orders/completed/${id}`);
  }

  /**
   * Updates store's details
   * @param storeInfo
   * @param storeOptions
   * @param id
   */
  updateMerchantData = async (storeInfo: any, storeOptions: any, id: any): Promise<any> => {
    try {
      return await this.commonService.putRequest('stores/' + id, {storeInfo, storeOptions});
    } catch (e) {
      throw e;
    }
  }


  // The following requests are features not currently used
  getRequests = async (): Promise<any> => {
    try {
      return await this.commonService.getRequest('stores/request');
    } catch (e) {
      throw e;
    }
  }

  getRegistrationById = async (id: any): Promise<any> => {
    // try {
    //   return await this.commonService.getRequest('' + id);
    // } catch (e) {
    //   throw e;
    // }
  }

  declineRegistrationRequest = async (id: any): Promise<any> => {
    // try {
    //   return await this.commonService.deleteRequest('' + id)
    // } catch (e) {
    //   throw e;
    // }
  }

  acceptRegistrationRequest = async (data: any): Promise<any> => {
    // try {
    //   return await this.commonService.postRequest('', data);
    // } catch (e) {
    //   throw e;
    // }
  }

}
