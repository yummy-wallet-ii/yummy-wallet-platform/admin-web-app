import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LoadingService {
  loading = true;
  loadingSubject: BehaviorSubject<boolean>;

  constructor() {
    this.loadingSubject = new BehaviorSubject<boolean>(true);
  }

  setLoading(state: boolean): void {
    this.loadingSubject.next(state);
  }

  observe(): Observable<boolean> {
    return this.loadingSubject.asObservable();
  }
}
