export interface RequestItem{
  id: number;
  taxNumber: string;
  legalName: string;
  cityName: string;
  headquarters: string;
  email: string;
  telephone: string;
  requestDate: string;
}
