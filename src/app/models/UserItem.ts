export interface UserItem {
  id: number;
  identifier: string;
  firstName: string;
  lastName: string;
  provider: string;
}
