export interface User {
  id: number;
  identifier: string;
  roleId: number;
};
