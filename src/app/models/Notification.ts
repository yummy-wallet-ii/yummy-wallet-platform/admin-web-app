export interface Notification {
  id: number;
  title: string;
  body: string;
  dateCreated?: string;
}
