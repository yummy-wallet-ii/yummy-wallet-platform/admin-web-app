export interface Customer {
  id: number;
  identifier: string;
  firstName: string;
  lastName: string;
  roleId: number;
  provider: string;
  creditCardNumber: string;
};
