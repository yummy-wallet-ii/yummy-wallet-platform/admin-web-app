//not used
export interface MerchantData {
  id: number;
  brandName: string;
  address: string;
  city: string;
  zipCode: string;
  country: string;
  latitude: string;
  longitude: string;
  greekDescription: string;
  englishDescription: string;
  email: string;
  phoneNumber: string;
  website: string;
  facebook: string;
  instagram: string;
  twitter: string;
  officialBrandName: string;
  officialDescription: string;
  tax: number;
  afm: string;
  doy: string;
  baseAddress: string;
  cashback: boolean;
  returnPercentage: number;
  ratioYummy: number;
  ratioMerchant: number;
  yummyCommission: number;
  clientCommission: number;
  stamps: boolean;
  iBankPay: boolean;
  policies: [{
    greekPolicy: string;
    englishPolicy: string;
  }];
  schedule: [{day: string; open: string; close: string; }];
}
