export interface MerchantItem {
  id: number;
  legalName: string;
  cityName: string;
  categoryDescription: string;
  email: string;
}
