import { Component, OnInit } from '@angular/core';
import {TableColumn} from '../../../models/TableColumn';
import {LoadingService} from '../../../services/loading.service';
import {ToastMessagesService} from '../../../services/toast-messages.service';
import {AdminService} from '../../../services/admin.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-stores',
  templateUrl: './stores.component.html',
  styleUrls: ['./stores.component.scss']
})
export class StoresComponent implements OnInit {
  stores: any[] = [];
  columns: TableColumn[] = [
    {
      label: 'Αναγνωριστικό καταστήματος',
      key: 'id',
      type: 'text',
      displayOnTable: true
    },
    {
      label: 'Επωνυμία',
      key: 'legalName',
      type: 'text',
      displayOnTable: true
    },
    {
      label: 'Email',
      key: 'email',
      type: 'text',
      displayOnTable: true
    },
    {
      label: 'Δραστηριότητα',
      key: 'categoryDescription',
      type: 'text',
      displayOnTable: true
    },
    {
      label: 'Πόλη',
      key: 'cityName',
      type: 'text',
      displayOnTable: true
    },
    {
      label: 'Τηλέφωνο',
      key: 'telephone',
      type: 'text',
      displayOnTable: true
    },
    {
      label: 'Α.Φ.Μ',
      key: 'taxNumber',
      type: 'text',
      displayOnTable: true
    },
    {
      label: 'Υπόλοιπο (€)',
      key: 'bucket',
      type: 'text',
      displayOnTable: true
    },
    {
      label: 'Ενεργό',
      key: 'isActive',
      type: 'boolean',
      displayOnTable: true,
      alignment: 'center'
    }
  ];
  selectedRowData: any;
  contextMenuItems = [
    { text: 'Προβολή καταστήματος', icon: 'info', disabled: false },
    { text: 'Απενεργοποίηση καταστήματος', icon: 'clear', disabled: false },
    { text: 'Ενεργοποίηση καταστήματος', icon: 'check', disabled: false },
  ];

  constructor(public loadingService: LoadingService,
              private adminService: AdminService,
              public router: Router,
              public toastService: ToastMessagesService, ) { }

  async ngOnInit(): Promise<void> {
    await this.fetchStores();
  }

  getContextMenuItems(): any {
    if(this.selectedRowData) {
     if (this.selectedRowData.isActive) {
       this.contextMenuItems[2].disabled = true;
       this.contextMenuItems[1].disabled = false;
     }  else {
       this.contextMenuItems[2].disabled = false;
       this.contextMenuItems[1].disabled = true;
     }
    } else {
      this.contextMenuItems.map(item => item.disabled = false);
      return this.contextMenuItems;
    }
  }

  contextMenuEvent(e: any): void {
    this.selectedRowData = e;
  }

  async navigateTo(e: any): Promise<any> {
    this.selectedRowData = e;
    await this.navigate();
  }

  async itemClick(e: any): Promise<void> {
    switch (e.itemIndex) {
      case 0:
        await this.navigate();
        break;
      case 1:
        await this.updateStatusActivation(0);
        break;
      case 2:
        await this.updateStatusActivation(1);
        break;
    }
  }

  async updateStatusActivation(status: number): Promise<void> {
    try {
      this.loadingService.setLoading(true);
      await this.adminService.updateStatusActivity(this.selectedRowData.id, status);
      const statusText = status ? 'ενεργοποιήθηκε' : 'απενεργοποιήθηκε';
      this.toastService.toastMessages(`Το κατάστημα ${statusText} με επιτυχία`);
      await this.fetchStores();
    } catch (e) {
      this.toastService.toastErrorMessage(e.message);
    } finally {
      this.loadingService.setLoading(false);
    }
  }
  async navigate(): Promise<void> {
    const id = this.selectedRowData.id;
    await this.router.navigate([`dashboard/merchant/data/${id}`]);
  }

  async fetchStores(): Promise<void> {
    try {
      this.loadingService.setLoading(true);
      this.stores = await this.adminService.getMerchants();
    } catch (e) {
      this.toastService.toastErrorMessage(e.message);
    } finally {
      this.loadingService.setLoading(false);
    }
  }

}
