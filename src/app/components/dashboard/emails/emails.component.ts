import { Component, OnInit } from '@angular/core';
import {LoadingService} from '../../../services/loading.service';
import {AdminService} from '../../../services/admin.service';
import {ToastMessagesService} from '../../../services/toast-messages.service';
import {TableColumn} from '../../../models/TableColumn';

@Component({
  selector: 'app-emails',
  templateUrl: './emails.component.html',
  styleUrls: ['./emails.component.scss']
})
export class EmailsComponent implements OnInit {
  emails: any[] = [];
  columns: TableColumn[] = [
    {
      label: 'Αναγνωριστικό',
      key: 'id',
      type: 'text',
      displayOnTable: true
    },
    {
      label: 'Τίτλος Εmail',
      key: 'title',
      type: 'text',
      displayOnTable: true
    },
    {
      label: 'Κατάστημα',
      key: 'store',
      type: 'text',
      displayOnTable: true
    },
    {
      label: 'Καταναλωτής',
      key: 'customerName',
      type: 'text',
      displayOnTable: true
    },
    {
      label: 'Ημ. Αποστολής',
      key: 'sendDate',
      type: 'date',
      displayOnTable: true
    },
];
  constructor(public loadingService: LoadingService,
              private adminService: AdminService,
              public toastService: ToastMessagesService) { }

  async ngOnInit(): Promise<void> {
    try {
      this.loadingService.setLoading(true);
      await this.fetchEmails();
    } catch (e) {
      this.toastService.toastErrorMessage(e.error.message);
    } finally {
      this.loadingService.setLoading(false);
    }
  }

  async fetchEmails(): Promise<void> {
    this.emails = await this.adminService.fetchEmailLogs();
  }

}
