import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  @Input() title = '';
  @Input() icon = '';
  @Output() actionEmitter: EventEmitter<string> = new EventEmitter<string>();
  @Input() action = '';
  @Input() actionName = '';
  constructor() { }

  ngOnInit(): void {
  }

  raiseActionEvent(): void {
    this.actionEmitter.emit(this.action);
  }
}
