import {Component, OnInit} from '@angular/core';
import {TableColumn} from '../../../models/TableColumn';
import {LoadingService} from '../../../services/loading.service';
import {ToastMessagesService} from '../../../services/toast-messages.service';
import {CommonService} from '../../../services/common.service';
import {MatDialog} from '@angular/material/dialog';
import {CreateInvoiceDialogComponent} from './create-invoice-dialog/create-invoice-dialog.component';
import {Router} from '@angular/router';
import {AdminService} from "../../../services/admin.service";

@Component({
  selector: 'app-invoices',
  templateUrl: './invoices.component.html',
  styleUrls: ['./invoices.component.scss']
})
export class InvoicesComponent implements OnInit {
  invoices: any = [];
  selectedRowData: any;
  columns: TableColumn[] = [
    {
      label: 'Αρ. Τιμολογίου',
      key: 'id',
      type: 'text',
      displayOnTable: true
    },
    {
      label: 'Επωνυμία',
      key: 'store_name',
      type: 'text',
      displayOnTable: true
    },
    {
      label: 'Δραστηριότητα',
      key: 'category',
      type: 'text',
      displayOnTable: true
    },
    {
      label: 'Α.Φ.Μ',
      key: 'afm',
      type: 'text',
      displayOnTable: true
    },
    {
      label: 'Διεύθυνση',
      key: 'address',
      type: 'text',
      displayOnTable: true
    },
    {
      label: 'Τηλέφωνο',
      key: 'phone',
      type: 'text',
      displayOnTable: true
    },
    {
      label: 'Email',
      key: 'email',
      type: 'text',
      displayOnTable: true
    },
    {
      label: 'Ποσό (€)',
      key: 'price',
      type: 'text',
      displayOnTable: true,
      alignment: 'center'
    },
    {
      label: 'Φ.Π.Α',
      key: 'price_tax',
      type: 'text',
      displayOnTable: true,
      alignment: 'center'
    },
    {
      label: 'Σύνολο (€)',
      key: 'final_price',
      type: 'text',
      displayOnTable: true,
      alignment: 'center'
    },
    {
      label: 'Ημερομηνία έκδοσης',
      key: 'dateCreated',
      type: 'date',
      displayOnTable: true
    }
  ];
  contextMenuItems = [
    { text: 'Προβολή τιμολογίου', icon: 'info' },
    { text: 'Έκδοση τιμολογίου', icon: 'print' },
  ];

  constructor(private loadingService: LoadingService,
              private commonService: CommonService,
              public dialog: MatDialog,
              public router: Router,
              private adminService: AdminService,
              private toastService: ToastMessagesService) {
  }

  async ngOnInit(): Promise<void> {
    try {
      this.loadingService.setLoading(true);
      this.invoices = await this.fetchInvoices();
    } catch (e) {
      this.toastService.toastErrorMessage(e.message);
    } finally {
      this.loadingService.setLoading(false);
    }
  }

  contextMenuEvent(e: any): void {
    this.selectedRowData = e;
  }

  async itemClick(e: any): Promise<void> {
    switch (e.itemIndex) {
      case 0:
        await this.navigate();
        break;
      case 1:
        await this.printPDF();
    }
  }

  async printPDF(e?: any): Promise<void> {
    try {
      if (e) { this.selectedRowData = e;}
      this.loadingService.setLoading(true);
      const response = await this.adminService.downloadInvoice(this.selectedRowData.id);
      const link = document.createElement('a');
      link.target = '_blank';
      link.href = URL.createObjectURL(
        new Blob([this.commonService.base64ToBlob(response)], {
          type: 'application/pdf'
        })
      );
      link.click();
    } catch (e) {
      this.toastService.toastErrorMessage(e.message ? e.message : e.serverMessage);
    } finally {
      this.loadingService.setLoading(false);
    }
  }

  async navigate(e?: any): Promise <void> {
    if (e) { this.selectedRowData = e; }
    await this.router.navigate([`dashboard/invoices/info/${this.selectedRowData.id}`]);
  }

  async fetchInvoices(): Promise<any> {
    return await this.commonService.getRequest('invoices');
  }

  actionEmitter($event: string): void {
    const dialog = this.dialog.open(CreateInvoiceDialogComponent, {
      hasBackdrop: true,
      disableClose: true
    });

    dialog.afterClosed().subscribe(async data => {
      if (data) {
        await this.createInvoice(data);
        this.toastService.toastMessages('Η τιμολόγηση δημιουργήθηκε με επιτυχία');
        this.invoices = await this.fetchInvoices();
      }
    });
  }

  async createInvoice(invoice: any): Promise<void> {
    try {
      this.loadingService.setLoading(true);
      await this.commonService.postRequest('invoices', {invoice});
    } catch (e) {
      this.toastService.toastErrorMessage(e.message);
    } finally {
      this.loadingService.setLoading(false);
    }
  }
}
