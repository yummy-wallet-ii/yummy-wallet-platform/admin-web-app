import {Component, Inject, OnInit} from '@angular/core';
import {CommonService} from '../../../../services/common.service';
import {LoadingService} from '../../../../services/loading.service';
import {ToastMessagesService} from '../../../../services/toast-messages.service';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {AdminService} from "../../../../services/admin.service";

@Component({
  selector: 'app-create-invoice-dialog',
  templateUrl: './create-invoice-dialog.component.html',
  styleUrls: ['./create-invoice-dialog.component.scss']
})
export class CreateInvoiceDialogComponent implements OnInit {
  fields: any[] = [];
  priceFields: any[] = [];
  stores: any[] = [];
  categories: any[] = [];
  selectedStoreId: number | undefined;
  isLoading: boolean;

  constructor(private commonService: CommonService,
              @Inject(MAT_DIALOG_DATA) public data: any,
              private dialogRef: MatDialogRef<CreateInvoiceDialogComponent>,
              public toastService: ToastMessagesService,
              private adminService: AdminService,
              public loadingService: LoadingService) {
    this.isLoading = true;
    this.fields = [
      {
        label: 'Δραστηριότητα',
        key: 'category',
        value: '',
      },
      {
        label: 'Α.Φ.Μ',
        key: 'afm',
        value: '',
      },
      {
        label: 'Διεύθυνση',
        key: 'address',
        value: '',
      },
      {
        label: 'Τηλέφωνο',
        key: 'phone',
        value: '',
      },
      {
        label: 'Email',
        key: 'email',
        value: '',
      }
    ];
    this.priceFields = [{
      label: 'Ποσό',
      key: 'price',
      value: '',
    },
      {
        label: 'Φ.Π.Α',
        key: 'price_tax',
        value: '',
      },
      {
        label: 'Σύνολο',
        key: 'final_price',
        value: '',
      }];
  }

  isButtonDisabled(): boolean {
    return !(this.selectedStoreId && this.priceFields[0].value);
  }

  async ngOnInit(): Promise<void> {
    try {
      // this.loadingService.setLoading(true);
      await this.fetchStores();
    } catch (e) {
      console.log(e);
      this.toastService.toastErrorMessage(e.message);
    } finally {
      this.isLoading = false;
    }
  }

  async fetchStores(): Promise<void> {
    this.stores = await this.adminService.getMerchants();
    console.log(this.data);
    if (this.data) {
      this.selectedStoreId = this.data.selectedStore;
      this.setData();
      this.priceFields[0].value = this.data.amount;
      this.calculate();
    }
  }

  async fetchCategories(): Promise<void> {
    this.categories = await this.commonService.getRequest('categories');
  }

  calculate(): void {
    if (this.priceFields[0].value) {
      this.priceFields[1].value = +this.priceFields[0].value * 0.024;
      this.priceFields[2].value = +this.priceFields[0].value + +this.priceFields[1].value;
    }
  }

  create(): void {
    const invoice = {
      store_id: this.selectedStoreId,
      price: +this.priceFields[0].value,
      price_tax: +this.priceFields[1].value
    };
    this.close(invoice);
  }

  close(data?: any): void {
    this.dialogRef.close(data);
  }

  setData(): void {
    const store = this.stores.find((s: any) => s.id === this.selectedStoreId);
    if (store) {
      this.fields[0].value = store.categoryDescription;
      this.fields[1].value = store.taxNumber;
      this.fields[2].value = store.headquarters;
      this.fields[3].value = store.telephone;
      this.fields[4].value = store.email;
    }
  }
}
