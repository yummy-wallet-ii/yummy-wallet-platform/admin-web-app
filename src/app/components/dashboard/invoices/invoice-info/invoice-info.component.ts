import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {LoadingService} from '../../../../services/loading.service';
import {AdminService} from '../../../../services/admin.service';
import {ToastMessagesService} from '../../../../services/toast-messages.service';
// @ts-ignore
import html2pdf from 'html2pdf.js';
import {CommonService} from '../../../../services/common.service';

@Component({
  selector: 'app-invoice-info',
  templateUrl: './invoice-info.component.html',
  styleUrls: ['./invoice-info.component.scss']
})
export class InvoiceInfoComponent implements OnInit {
  @ViewChild('invoiceContent') invoiceContent: ElementRef | undefined;

  invoiceId: any;
  invoice: any;

  constructor(private route: ActivatedRoute,
              public loadingService: LoadingService,
              private adminService: AdminService,
              private commonService: CommonService,
              public toastService: ToastMessagesService) {
  }

  async ngOnInit(): Promise<void> {
    try {
      this.loadingService.setLoading(true);
      this.invoiceId = await this.getInvoiceId();
      this.invoice = await this.fetchInvoice();
    } catch (e) {
      this.toastService.toastErrorMessage(e.message);
    } finally {
      this.loadingService.setLoading(false);
    }
  }

  async fetchInvoice(): Promise<any> {
    return await this.adminService.getInvoiceById(this.invoiceId);
  }

  async printPDF(): Promise<void> {
    try {
      const response = await this.adminService.downloadInvoice(this.invoiceId);
      const link = document.createElement('a');
      link.target = '_blank';
      link.href = URL.createObjectURL(
        new Blob([this.commonService.base64ToBlob(response)], {
          type: 'application/pdf'
        })
      );
      link.click();
    } catch (e) {
      this.toastService.toastErrorMessage(e.message ? e.message : e.serverMessage);
    }
  }

  async getInvoiceId(): Promise<number> {
    return new Promise(resolve => {
      this.route.params.subscribe(async (params: any) => {
        resolve(+params.id);
      });
    });
  }

}
