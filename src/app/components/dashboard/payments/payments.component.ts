import { Component, OnInit } from '@angular/core';
import {TableColumn} from '../../../models/TableColumn';
import {LoadingService} from '../../../services/loading.service';
import {ToastMessagesService} from '../../../services/toast-messages.service';
import {CommonService} from '../../../services/common.service';
import {AdminService} from '../../../services/admin.service';
import {MatDialog} from '@angular/material/dialog';
import {CreateInvoiceDialogComponent} from '../invoices/create-invoice-dialog/create-invoice-dialog.component';

@Component({
  selector: 'app-payments',
  templateUrl: './payments.component.html',
  styleUrls: ['./payments.component.scss']
})
export class PaymentsComponent implements OnInit {
  payments: any[] = [];
  columns: TableColumn[] = [{
    label: 'A/A',
    key: 'id',
    type: 'text',
    displayOnTable: true
  },
    {
      label: 'Ημ. Πληρωμής',
      key: 'transactionDate',
      type: 'date',
      displayOnTable: true
    },
    {
      label: 'Κατάστημα',
      key: 'store',
      type: 'text',
      displayOnTable: true
    },
    {
      label: 'Email Καταστήματος',
      key: 'email',
      type: 'text',
      displayOnTable: true
    },
    {
      label: 'Ποσό πληρωμής (€)',
      key: 'amount',
      type: 'text',
      displayOnTable: true,
      alignment: 'center'
    },
    {
      label: 'Αναγνωριστικό πληρωμής Stripe',
      key: 'stripeTransactionId',
      type: 'text',
      displayOnTable: true,
      alignment: 'center'
    },
    {
      label: 'Αναγνωριστικό Συναλλαγής BlockChain',
      key: 'blockChainTransactionId',
      type: 'text',
      displayOnTable: true,
      alignment: 'center'
    },
  ];
  constructor(
    public loadingService: LoadingService,
    public toastService: ToastMessagesService,
    public dialog: MatDialog,
    private commonService: CommonService,
    private adminService: AdminService
  ) { }

  async ngOnInit(): Promise<void> {
    await this.fetchPayments();
  }

  rowDoubleClickEvent(event: any): void {
    console.log(event);
    const dialog = this.dialog.open(CreateInvoiceDialogComponent, {
      hasBackdrop: true,
      disableClose: true,
      data: {
        selectedStore: event.storeId,
        amount: event.amount
      }
    });

    dialog.afterClosed().subscribe(async data => {
      if (data) {
        await this.createInvoice(data);
        this.toastService.toastMessages('Η τιμολόγηση δημιουργήθηκε με επιτυχία. Μπορείτε να τη δείτε στη λίστα των τιμολογήσεών σας');
      }
    });
  }

  async createInvoice(invoice: any): Promise<void> {
    try {
      this.loadingService.setLoading(true);
      await this.commonService.postRequest('invoices', {invoice});
    } catch (e) {
      this.toastService.toastErrorMessage(e.message);
    } finally {
      this.loadingService.setLoading(false);
    }
  }

  async fetchPayments(): Promise<void> {
    try {
      this.loadingService.setLoading(true);
      this.payments = await this.adminService.getPayments();
    } catch (e) {
      this.toastService.toastErrorMessage(e.message);
    } finally {
      this.loadingService.setLoading(false);
    }
  }
}
