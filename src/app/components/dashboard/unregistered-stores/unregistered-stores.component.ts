import { Component, OnInit } from '@angular/core';
import {TableColumn} from '../../../models/TableColumn';
import {LoadingService} from '../../../services/loading.service';
import {AdminService} from '../../../services/admin.service';
import {ToastMessagesService} from '../../../services/toast-messages.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-unregistered-stores',
  templateUrl: './unregistered-stores.component.html',
  styleUrls: ['./unregistered-stores.component.scss']
})
export class UnregisteredStoresComponent implements OnInit {
  stores: any[] = [];
  selectedRowData: any;
  contextMenuItems = [
    { text: 'Προβολή καταστήματος', icon: 'info' },
    { text: 'Εγγραφή καταστήματος', icon: 'check' },
  ];
  columns: TableColumn[] = [
    {
      label: 'Αναγνωριστικό καταστήματος',
      key: 'id',
      type: 'text',
      displayOnTable: true
    },
    {
      label: 'Επωνυμία',
      key: 'legalName',
      type: 'text',
      displayOnTable: true
    },
    {
      label: 'Email',
      key: 'email',
      type: 'text',
      displayOnTable: true
    },
    {
      label: 'Δραστηριότητα',
      key: 'categoryDescription',
      type: 'text',
      displayOnTable: true
    },
    {
      label: 'Πόλη',
      key: 'cityName',
      type: 'text',
      displayOnTable: true
    },
    {
      label: 'Τηλέφωνο',
      key: 'telephone',
      type: 'text',
      displayOnTable: true
    },
    {
      label: 'Α.Φ.Μ',
      key: 'taxNumber',
      type: 'text',
      displayOnTable: true
    },
    {
      label: 'Ημ. Αίτησης',
      key: 'requestDate',
      type: 'date',
      displayOnTable: true
    }
  ];

  constructor(public loadingService: LoadingService,
              private adminService: AdminService,
              public router: Router,
              public toastService: ToastMessagesService) { }

  async ngOnInit(): Promise<void> {
    await this.fetchStores();
  }

  contextMenuEvent(e: any): void {
    this.selectedRowData = e;
  }

  async itemClick(e: any): Promise<void> {
    switch (e.itemIndex) {
      case 0:
        await this.navigate();
        break;
      case 1:
        await this.registerStore();
    }
  }

  async navigate(): Promise<void> {
    const id = this.selectedRowData.id;
    await this.router.navigate([`dashboard/merchant/data/${id}`]);
  }

  async navigateTo(e: any): Promise<any> {
    this.selectedRowData = e;

    await this.navigate();
  }

  async registerStore(): Promise<void> {
    try {
      this.loadingService.setLoading(true);
      await this.adminService.registerStore(this.selectedRowData.id);
      this.toastService.toastMessages('Το κατάστημα εγγράφηκε με επιτυχία');
      await this.fetchStores();
    } catch (e) {
      this.toastService.toastErrorMessage(e.message);
    } finally {
      this.loadingService.setLoading(false);
    }
  }

  async fetchStores(): Promise<void> {
    try {
      this.loadingService.setLoading(true);
      this.stores = await this.adminService.getRequests();
    } catch (e) {
      this.toastService.toastErrorMessage(e.message);
    } finally {
      this.loadingService.setLoading(false);
    }
  }
}
