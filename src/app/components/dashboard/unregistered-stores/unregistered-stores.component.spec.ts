import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UnregisteredStoresComponent } from './unregistered-stores.component';

describe('UnregisteredStoresComponent', () => {
  let component: UnregisteredStoresComponent;
  let fixture: ComponentFixture<UnregisteredStoresComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UnregisteredStoresComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UnregisteredStoresComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
