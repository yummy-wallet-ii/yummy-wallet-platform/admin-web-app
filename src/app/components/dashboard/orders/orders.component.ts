import { Component, OnInit } from '@angular/core';
import {LoadingService} from '../../../services/loading.service';
import {ToastMessagesService} from '../../../services/toast-messages.service';
import {AdminService} from '../../../services/admin.service';
import {TableColumn} from '../../../models/TableColumn';
import {CommonService} from '../../../services/common.service';

@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.scss']
})
export class OrdersComponent implements OnInit {
  orders: any[] = [];
  columns: TableColumn[] = [{
    label: 'A/A',
    key: 'id',
    type: 'text',
    displayOnTable: true
  },
    {
      label: 'Ημ. Παραγγελίας',
      key: 'orderDate',
      type: 'date',
      displayOnTable: true
    },
    {
      label: 'Κατάστημα',
      key: 'store',
      type: 'text',
      displayOnTable: true
    },
    {
      label: 'Ποσό πληρωμής (€)',
      key: 'price',
      type: 'text',
      displayOnTable: true,
      alignment: 'center'
    },
    {
      label: 'Ημ. Ολοκλήρωσης',
      key: 'completedAt',
      type: 'date',
      displayOnTable: true
    },
   ];
  constructor(public loadingService: LoadingService,
              public toastService: ToastMessagesService,
              private commonService: CommonService,
              private adminService: AdminService) { }

  async ngOnInit(): Promise<void> {
    try {
      this.loadingService.setLoading(true);
      await this.fetchOrders();
    } catch (e) {
      this.toastService.toastErrorMessage(e.message);
    } finally {
      this.loadingService.setLoading(false);
    }
  }

  async fetchOrders(): Promise<void> {
    const orders = await this.adminService.getOrders();
    const finalOrders: any = [];
    orders.forEach((order: any) => {
      const date = this.commonService.formatDate(order.orderDate);
      const completedDate = this.commonService.formatDate(order.completedAt);
      const model = {
        id: order.id,
        orderDate: date,
        store: order.base_store.legalName,
        price: order.price,
        completedAt: completedDate
      };
      finalOrders.push(model);
    });
    this.orders = finalOrders;
  }

}
