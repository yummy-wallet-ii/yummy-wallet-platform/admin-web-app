import {Component, OnInit} from '@angular/core';
import {LoadingService} from '../../../services/loading.service';
import {AdminService} from '../../../services/admin.service';
import {ToastMessagesService} from '../../../services/toast-messages.service';
import {ChartType} from '../../../models/ChartType';
import {CommonService} from '../../../services/common.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit {
  stores: any[] = [];
  requestedStores: any[] = [];
  users: any[] = [];
  emails: any[] = [];
  notifications: any[] = [];
  orders: any[] = [];
  charts: any[] = [{
    id: 1,
    title: 'Eγγραφές καταστημάτων',
    data: [],
    chartType: ChartType.Line
  },
    {
      id: 2,
      title: 'Παραγγελίες ανα ημέρα',
      data: [],
      chartType: ChartType.Line
    },
    {
      id: 3,
      title: 'Καταστήματα με τις περισσότερες συνολικές παραγγελίες',
      data: [],
      chartType: ChartType.Bar
    },
    {
      id: 4,
      title: 'Εξέλιξη χρηστών',
      data: [],
      chartType: ChartType.Line
    }];
  cardInfo = [{
    id: 1,
    title: 'Εγγεγραμμένα καταστήματα',
    value: 0,
    icon: 'store',
    color: '#4caf5080',
    link: 'dashboard/merchants'
  },
    {
      id: 2,
      title: 'Αιτήματα εγγραφής',
      value: 0,
      icon: 'storefront',
      color: '#09477180',
      link: 'dashboard/requests'
    },
    {
      id: 3,
      title: 'Σύνολο χρηστών',
      value: 0,
      icon: 'person',
      color: '#74dccc80',
      link: 'dashboard/users'
    },
    {
      id: 3,
      title: 'Σύνολο παραγγελιών',
      value: 0,
      icon: 'sticky_note_2',
      color: '#0e263780',
      link: 'dashboard/orders'
    },
    {
      id: 4,
      title: 'Σύνολο email',
      value: 0,
      icon: 'mail',
      color: '#094771',
      link: 'dashboard/emails'
    },
    {
      id: 5,
      title: 'Σύνολο ειδοποιήσεων',
      value: 0,
      icon: 'notifications',
      color: 'pink',
      link: 'dashboard/notifications'
    }];

  constructor(public loadingService: LoadingService,
              private adminService: AdminService,
              public router: Router,
              private commonService: CommonService,
              public toastService: ToastMessagesService) {
  }

  async ngOnInit(): Promise<void> {
    try {
      this.loadingService.setLoading(true);
      await this.fetchStores();
      await this.fetchRequests();
      await this.fetchUsers();
      await this.fetchAllOrders();
      await this.fetchEmails();
      await this.fetchNotifications();
      console.log(this.notifications);
      this.populateCards();
      this.populateCharts();
    } catch (e) {
      this.toastService.toastErrorMessage(e.message);
    } finally {
      this.loadingService.setLoading(false);
    }
  }

  async fetchEmails(): Promise<void> {
    this.emails = await this.adminService.fetchEmailLogs();
  }
  async fetchNotifications(): Promise<void> {
    this.notifications = await this.adminService.fetchNotificationLogs();
  }

  populateCards(): void {
    this.cardInfo[0].value = this.stores.length;
    this.cardInfo[1].value = this.requestedStores.length;
    this.cardInfo[2].value = this.users.length;
    this.cardInfo[3].value = this.orders.length;
    this.cardInfo[4].value = this.emails.length;
    this.cardInfo[5].value = this.notifications.length;
  }

  populateCharts(): void {
    let ordersPerStore = [];
    const ordersPerDay = [];
    const storesCreated = [];
    const usersCreated = [];

    const groupedStores = this.commonService.groupBy(this.stores, 'createdAt');
    const groupedOrders = this.commonService.groupBy(this.orders, 'storeId');
    const groupedOrdersPerDay = this.commonService.groupBy(this.orders, 'orderDateOnly');
    const groupedUsers = this.commonService.groupBy(this.users, 'createdAt');
    // console.log(groupedOrdersPerDay);

    // tslint:disable-next-line:forin
    for (const key in groupedStores) {
      const model = {
        label: key,
        value: groupedStores[key].length
      };
      storesCreated.push(model);
    }
    this.charts[0].data = storesCreated;

    // tslint:disable-next-line:forin
    for (const key in groupedOrders) {
      const store = this.stores.find(s => s.id === +key);
      if (store) {
        const model = {
          label: store.legalName,
          value: groupedOrders[key].length
        };
        ordersPerStore.push(model);
      }
    }
    ordersPerStore = ordersPerStore.sort(
      (p1, p2) => (p1.value < p2.value) ? -1 : (p1.value > p2.value) ? 1 : 0);
    this.charts[2].data = ordersPerStore;

    // tslint:disable-next-line:forin
    for (const key in groupedOrdersPerDay) {
      const model = {
        label: key,
        value: groupedOrdersPerDay[key].length
      };
      ordersPerDay.push(model);
    }
    this.charts[1].data = ordersPerDay;

    // tslint:disable-next-line:forin
    for (const key in groupedUsers) {
      const model = {
        label: key,
        value: groupedUsers[key].length
      };
      usersCreated.push(model);
    }
    this.charts[3].data = usersCreated;
  }

  async fetchStores(): Promise<void> {
    this.stores = await this.adminService.getMerchants();
  }

  async fetchRequests(): Promise<void> {
    this.requestedStores = await this.adminService.getRequests();
  }

  async fetchUsers(): Promise<void> {
    this.users = await this.adminService.getUsers();
  }

  async fetchAllOrders(): Promise<void> {
    const orders = await this.adminService.getOrders();
    for (const order of orders) {
      const tempDate = order.orderDate.split(' ');
      order.orderDateOnly = tempDate[0];
    }
    this.orders = orders;
  }

  async navigateTo(path: string): Promise<void> {
    await this.router.navigate([path]);
  }
}
