import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SideBarItem } from '../../models/SideBarItem';
import { LogoutDialogComponent } from '../dialogs/logout-dialog/logout-dialog.component';
import { MatDialog } from '@angular/material/dialog';
import {Subscription} from 'rxjs';
import {LoadingService} from '../../services/loading.service';
import {ToastMessagesService} from '../../services/toast-messages.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  isSidebarOpened: boolean;
  sideBarItems: SideBarItem[] = [];
  loading = true;
  subscription: Subscription;

  constructor(private router: Router,  private dialog: MatDialog,
              public loadingService: LoadingService,
              public toastService: ToastMessagesService) {
    this.isSidebarOpened = false;
    this.subscription = this.loadingService.observe().subscribe(
      (state) => {
        this.loading = state;
      },
      (error) => this.toastService.toastErrorMessage(error.message)
    );
  }

  ngOnInit(): void {
    this.initializeSideNav();
  }

  initializeSideNav(): void {
    const dashboard: SideBarItem = {
      title: 'Dashboard',
      openDialog: false,
      path: '/dashboard',
      icon: 'dashboard'
    };
    const users: SideBarItem = {
      title: 'Χρήστες',
      openDialog: false,
      path: '/dashboard/users',
      icon: 'people'
    };

    const merchants: SideBarItem = {
      title: 'Καταστήματα',
      openDialog: false,
      path: '/dashboard/merchants',
      icon: 'store'
    };

    const invoices: SideBarItem = {
      title: 'Τιμολογήσεις',
      openDialog: false,
      path: '/dashboard/invoices',
      icon: 'request_quote'
    };
    const orders: SideBarItem = {
      title: 'Παραγγελίες',
      openDialog: false,
      path: '/dashboard/orders',
      icon: 'shopping_cart'
    };

    const requests: SideBarItem = {
      title: 'Αιτήματα',
      openDialog: false,
      path: '/dashboard/requests',
      icon: 'note_add'
    };

    const payments: SideBarItem = {
      title: 'Πληρωμές',
      openDialog: false,
      path: '/dashboard/payments',
      icon: 'attach_money'
    };
    const emails: SideBarItem = {
      title: 'Emails',
      openDialog: false,
      path: '/dashboard/emails',
      icon: 'mail'
    };
    const notifications: SideBarItem = {
      title: 'Ειδοποιήσεις',
      openDialog: false,
      path: '/dashboard/notifications',
      icon: 'notifications'
    };
    const redeems: SideBarItem = {
      title: 'Αιτήματα εξαργύρωσης',
      openDialog: false,
      path: '/dashboard/redeem-requests',
      icon: 'attach_money'
    };
    this.sideBarItems.push(dashboard, merchants, requests, orders, payments, invoices, users, emails, notifications, redeems);

    // this.sideBarItems.push(users, merchants);
  }


  isActive(sideBarItem: any): boolean {
    return sideBarItem.path === this.router.url;
  }

  public showMenuButton(): boolean {
    return window.innerWidth < 1100;
  }

  public setSidebarMode(): any {
    return window.innerWidth < 1100 ? 'over' : 'side';
  }

  public showSidebar(): boolean {
    return window.innerWidth < 1100 ? this.isSidebarOpened : true;
  }

  public resetSidebar(): void {
    this.isSidebarOpened = false;
  }

  async action(sidebarItem: SideBarItem): Promise<void> {
    try {
      await this.router.navigate([sidebarItem.path]);
    } catch (e) {
      console.log(e);
    }

  }


  public async logout(): Promise<void> {
    const loggingOut = this.dialog.open(LogoutDialogComponent, {
      panelClass: 'no-padding-dialog',
    });
    loggingOut.afterClosed().subscribe(async (response: any) => {
      if (response) {
        localStorage.removeItem('token');
        await this.router.navigate(['login']);
      }
    });
  }


}

