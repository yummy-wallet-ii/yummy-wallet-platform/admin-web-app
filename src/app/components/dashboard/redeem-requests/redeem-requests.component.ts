import { Component, OnInit } from '@angular/core';
import {TableColumn} from '../../../models/TableColumn';
import {LoadingService} from '../../../services/loading.service';
import {AdminService} from '../../../services/admin.service';
import {ToastMessagesService} from '../../../services/toast-messages.service';

@Component({
  selector: 'app-redeem-requests',
  templateUrl: './redeem-requests.component.html',
  styleUrls: ['./redeem-requests.component.scss']
})
export class RedeemRequestsComponent implements OnInit {
  requests: any[] = [];
  selectedRowData: any;
  contextMenuItems = [
    { text: 'Ολοκλήρωση εξαργύρωσης', icon: 'check' },
    { text: 'Ακύρωση αιτήματος', icon: 'clear' },
  ];
  columns: TableColumn[] = [
    {
      label: 'Αναγνωριστικό Αίτησης',
      key: 'id',
      type: 'text',
      displayOnTable: true
    },
    {
      label: 'Όνομα',
      key: 'customerFirstName',
      type: 'text',
      displayOnTable: true
    },
    {
      label: 'Επώνυμο',
      key: 'customerLastName',
      type: 'text',
      displayOnTable: true
    },
    {
      label: 'Email',
      key: 'customerEmail',
      type: 'text',
      displayOnTable: true
    },
    {
      label: 'Τηλέφωνο',
      key: 'customerMobile',
      type: 'text',
      displayOnTable: true
    },
    {
      label: 'I.B.A.N',
      key: 'customerIBAN',
      type: 'text',
      displayOnTable: true
    },
    {
      label: 'Πλήθος νομισμάτων',
      key: 'amount',
      type: 'text',
      displayOnTable: true
    },
    {
      label: 'Κατάσταση',
      key: 'status',
      type: 'text',
      displayOnTable: true
    },
    {
      label: 'Ημ. Αίτησης',
      key: 'requestDate',
      type: 'date',
      displayOnTable: true
    },
    {
      label: 'Ημ. Ενημέρωσης',
      key: 'actionDate',
      type: 'date',
      displayOnTable: true
    },
  ];
  constructor(public loadingService: LoadingService,
              private adminService: AdminService,
              public toastService: ToastMessagesService) { }

  async ngOnInit(): Promise<void> {
    try {
      this.loadingService.setLoading(true);
      await this.fetchNotifications();
    } catch (e) {
      this.toastService.toastErrorMessage(e.error.message);
    } finally {
      this.loadingService.setLoading(false);
    }
  }

  async fetchNotifications(): Promise<void> {
    this.requests = await this.adminService.fetchRedeemRequests();
  }

  async itemClick(e: any): Promise<void> {
    switch (e.itemIndex) {
      case 0:
        await this.updateRequest('COMPLETED');
        break;
      case 1:
        await this.updateRequest('CANCELLED');
    }
  }

  contextMenuEvent(e: any): void {
    this.selectedRowData = e;
  }

  async updateRequest(status: string) {
    try {
      this.loadingService.setLoading(true);
      const request = {
        status
      }
      await this.adminService.updateRedeemRequest(this.selectedRowData.id, request);
      await this.fetchNotifications();
    } catch (e) {
      this.toastService.toastErrorMessage(e.message);
    } finally {
      this.loadingService.setLoading(false);
    }
  }

}
