import { AfterViewInit, Component, ViewChild } from "@angular/core";
import { MerchantItem } from "../../models/MerchantItem";
import { AdminService } from "../../services/admin.service";
import { ToastMessagesService } from "../../services/toast-messages.service";
import { Router } from "@angular/router";
import { MatPaginator } from "@angular/material/paginator";
import { MatTableDataSource } from "@angular/material/table";

@Component({
  selector: "app-merchants",
  templateUrl: "./merchants.component.html",
  styleUrls: ["./merchants.component.scss"]
})
export class MerchantsComponent implements AfterViewInit {

  @ViewChild(MatPaginator) paginator!: MatPaginator;
  dataSource: any;
  cardSource: any;
  cardSourceFiltered: any;
  hasData: boolean;
  isLoading: boolean;
  isEmpty: boolean;
  columns = [
    {
      columnDef: "id",
      header: "ID",
      cell: (element: MerchantItem) => `${element.id}`
    },
    {
      columnDef: "legalName",
      header: "Επωνυμία",
      cell: (element: MerchantItem) => `${element.legalName}`
    },
    {
      columnDef: "email",
      header: "Email",
      cell: (element: MerchantItem) => `${element.email}`
    },
    {
      columnDef: "cityName",
      header: "Πόλη",
      cell: (element: MerchantItem) => `${element.cityName}`
    },
    {
      columnDef: "categoryDescription",
      header: "Κατηγορία",
      cell: (element: MerchantItem) => `${element.categoryDescription}`
    }
  ];
  displayedColumns = this.columns.map(c => c.columnDef);

  constructor(private adminService: AdminService, public toastMessagesService: ToastMessagesService, public router: Router) {
    this.hasData = false;
    this.isLoading = true;
    this.isEmpty = false;
  }

  async ngAfterViewInit(): Promise<void> {
    await this.getMerchants();
  }

  /**
   * Fetches the stores and sets variables used for loading and responsive layout.
   */
  async getMerchants(): Promise<any> {
    try {
      this.dataSource = new MatTableDataSource<MerchantItem>(await this.adminService.getMerchants());
      this.hasData = this.dataSource.data.length > 0;
      this.isEmpty = !(this.dataSource.data.length > 0);
      this.cardSource = this.dataSource.data;
      this.cardSourceFiltered = [...this.cardSource];
      this.dataSource.paginator = this.paginator;
      this.dataSource.paginator._intl.itemsPerPageLabel = "Καταστήματα ανά σελίδα";
    } catch (e) {
      this.toastMessagesService.toastErrorMessage(e.message);
    } finally {
      this.isLoading = false;
    }
  }

  /**
   * Navigates to the store's  details page
   * @param id
   */
  async showBusinessData(id: any): Promise<void> {
    try {
     localStorage.setItem("registration", "false");
      await this.router.navigate([`dashboard/merchant/data/${id}`]);
    } catch (e) {
      this.toastMessagesService.toastErrorMessage(e.message);
    } finally {
    }
  }

  /**
   * Searches the table
   * @param event
   */
  search(event: any): void {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  /**
   * Searches the cards (used in smaller screens)
   * @param event
   */
  searchCard(event: any): void {
    const filterValue = (event.target as HTMLInputElement).value;
    if (filterValue === "") {
      this.cardSourceFiltered = [...this.cardSource];
    } else {
      const newFilterValue = filterValue.toLowerCase().trim();
      this.cardSourceFiltered = this.cardSource.filter((record: MerchantItem) => record.legalName.toLowerCase().includes(newFilterValue) ||
        record.email.toLowerCase().includes(newFilterValue));
    }
  }

}
