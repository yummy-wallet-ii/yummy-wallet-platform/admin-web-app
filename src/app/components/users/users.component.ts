import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import { AdminService } from '../../services/admin.service';
import { UserItem } from '../../models/UserItem';
import { ToastMessagesService } from '../../services/toast-messages.service';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import {Customer} from '../../models/Customer';
import {TableColumn} from '../../models/TableColumn';
import {LoadingService} from '../../services/loading.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {
  columns: TableColumn[] = [
    {
      label: 'Αναγνωριστικό χρήστη',
      key: 'id',
      type: 'text',
      displayOnTable: true
    },
    {
      label: 'Όνομα',
      key: 'firstName',
      type: 'text',
      displayOnTable: true
    },
    {
      label: 'Επίθετο',
      key: 'lastName',
      type: 'text',
      displayOnTable: true
    },
    {
      label: 'Email',
      key: 'identifier',
      type: 'text',
      displayOnTable: true
    },
  ];
  users: any[] = [];

  constructor(private adminService: AdminService, public toastMessagesService: ToastMessagesService,
              public loadingService: LoadingService) {
  }

  async ngOnInit(): Promise<void> {
    await this.getUsers();
  }


  async getUsers(): Promise<any> {
    try {
      this.loadingService.setLoading(true);
      this.users = await this.adminService.getUsers();
    } catch (e) {
      this.toastMessagesService.toastErrorMessage(e.message);
    } finally {
      this.loadingService.setLoading(false);
    }
  }
}
