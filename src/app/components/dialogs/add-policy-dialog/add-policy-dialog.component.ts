import { Component, Inject, OnInit } from "@angular/core";
import { MAT_DIALOG_DATA, MatDialogRef } from "@angular/material/dialog";

@Component({
  selector: 'app-add-policy-dialog',
  templateUrl: './add-policy-dialog.component.html',
  styleUrls: ['./add-policy-dialog.component.scss']
})
export class AddPolicyDialogComponent implements OnInit {

  constructor(@Inject(MAT_DIALOG_DATA) public data: any,
              private dialogRef: MatDialogRef<AddPolicyDialogComponent>) {
    dialogRef.disableClose = true;
  }

  ngOnInit(): void {
  }

}
