import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {LoadingService} from '../../services/loading.service';
import {ToastMessagesService} from '../../services/toast-messages.service';
import {AuthenticationService} from '../../services/authentication.service';
import {UserLoginModel} from '../../models/Business';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  loginModel: UserLoginModel = { email: '', password: '' };
  form: FormGroup;
  hide = true;

  constructor(
    private readonly formBuilder: FormBuilder,
    public router: Router,
    private loadingService: LoadingService,
    public toastMessagesService: ToastMessagesService,
    private authenticationService: AuthenticationService
  ) {
    this.form = this.formBuilder.group({
      identifier: ['', [Validators.required, Validators.email]],
      password: ['', Validators.required],
    });
  }

  ngOnInit(): void {
  }

  /**
   *The user logs in if the form is filled out correctly.
   * A special request is made for admins.
   */
  login = async () => {
    if (this.form.valid) {
      this.loadingService.setLoading(true);
      this.loginModel = this.form.getRawValue();
      try {
        const token = await this.authenticationService.login(this.loginModel);
        localStorage.setItem('token', token);
        await this.router.navigate(['dashboard']);
      } catch (e) {
        this.toastMessagesService.toastErrorMessage(e.message);
      } finally {
        this.loadingService.setLoading(false);
      }
    } else {
      console.log('Υπάρχει κάποιο πρόβλημα με τη φόρμα');
    }
  }
}
