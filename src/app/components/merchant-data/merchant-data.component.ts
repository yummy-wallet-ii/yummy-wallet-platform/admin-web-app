import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {ToastMessagesService} from '../../services/toast-messages.service';
import {AdminService} from '../../services/admin.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MatTable} from '@angular/material/table';
import {AddPolicyDialogComponent} from '../dialogs/add-policy-dialog/add-policy-dialog.component';
import {MatDialog} from '@angular/material/dialog';
import {DeletePolicyDialogComponent} from '../dialogs/delete-policy-dialog/delete-policy-dialog.component';
import {CommonService} from '../../services/common.service';
import {LoadingService} from '../../services/loading.service';
import {ChartType} from '../../models/ChartType';
import {TableColumn} from '../../models/TableColumn';
import {PushContentDialogComponent} from '../shared/push-content-dialog/push-content-dialog.component';
import {EmailCreatorDialogComponent} from '../shared/email-creator-dialog/email-creator-dialog.component';


@Component({
  selector: 'app-merchant-data',
  templateUrl: './merchant-data.component.html',
  styleUrls: ['./merchant-data.component.scss']
})
export class MerchantDataComponent implements OnInit {

  @ViewChild('customTemplate', {static: true}) customTemplate!: ElementRef;
  data: any;
  orders: any;
  customers: any = [];
  columns: TableColumn[] = [
    {
      label: 'Αναγνωριστικό χρήστη',
      key: 'id',
      type: 'text',
      displayOnTable: true
    },
    {
      label: 'Όνομα',
      key: 'firstName',
      type: 'text',
      displayOnTable: true
    },
    {
      label: 'Επώνυμο',
      key: 'lastName',
      type: 'text',
      displayOnTable: true
    },
    {
      label: 'Email',
      key: 'identifier',
      type: 'text',
      displayOnTable: true
    },
    {
      label: 'Τηλέφωνο',
      key: 'mobile',
      type: 'text',
      displayOnTable: true
    }];
  form: FormGroup;
  registration: any;
  merchantObject: any;
  imageSource: string | undefined;
  loadingImage: boolean;
  merchantId!: any;
  selectedRowData: any;
  storeName: string;
  cardInfo = [
    {
      id: 2,
      title: 'Yπόλοιπο',
      value: '0',
      icon: 'attach_money',
      color: '#09477180',
      link: 'dashboard/requests'
    },
    {
      id: 1,
      title: 'Έσοδα',
      value: 0,
      icon: 'attach_money',
      color: '#09477180',
      link: 'dashboard/requests'
    },
    {
      id: 3,
      title: 'Σύνολο χρηστών',
      value: 0,
      icon: 'person',
      color: '#74dccc80',
      link: 'dashboard/users'
    },
    {
      id: 3,
      title: 'Σύνολο παραγγελιών',
      value: 0,
      icon: 'sticky_note_2',
      color: '#0e263780',
      link: 'dashboard/orders'
    },
    {
      id: 4,
      title: 'Emails',
      value: 0,
      icon: 'email',
      color: '#094771',
      link: 'dashboard/orders'
    },
    {
      id: 5,
      title: 'Ειδοποιήσεις',
      value: 0,
      icon: 'notifications',
      color: 'pink',
      link: 'dashboard/orders'
    }
  ];
  charts: any[] = [{
    id: 1,
    title: 'Εξέλιξη παραγγελιών',
    data: [],
    chartType: ChartType.Line
  },
    {
      id: 2,
      title: 'Εξέλιξη νέων χρηστών',
      data: [],
      chartType: ChartType.Line
    }];
  emails: any;
  notifications: any;
  contextMenuItems = [
    { text: 'Αποστολή ειδοποίησης', icon: 'notifications', disabled: false },
    { text: 'Αποστολή Email', icon: 'email', disabled: false },
  ];
  constructor(
    private commonService: CommonService,
    private readonly formBuilder: FormBuilder,
    public dialog: MatDialog,
    public loadingService: LoadingService,
    public route: ActivatedRoute,
    public router: Router,
    public toastMessagesService: ToastMessagesService,
    private adminService: AdminService) {
    this.storeName = '';
    this.loadingImage = false;

    this.form = this.formBuilder.group({
        legalName: ['', Validators.required],
        street: ['', Validators.required],
        cityName: ['', Validators.required],
        postalCode: ['', [Validators.required, Validators.pattern('^[0-9]*$'), Validators.maxLength(5), Validators.minLength(5)]],
        countryName: ['', Validators.required],
        latitude: ['', [Validators.required, Validators.pattern('^[0-9]*\\.[0-9]+$')]],
        longitude: ['', [Validators.required, Validators.pattern('^[0-9]*\\.[0-9]+$')]],
        descriptionEl: ['', null],
        descriptionEn: ['', null],

        email: ['', [Validators.required, Validators.email]],
        telephone: ['', [Validators.required, Validators.pattern('^[0-9]*$'), Validators.maxLength(10), Validators.minLength(10)]],
        website: ['', Validators.required],
        facebook: ['', Validators.required],
        instagram: ['', Validators.required],
        twitter: ['', Validators.required],

        taxLegalName: ['', Validators.required],
        taxProfession: ['', Validators.required],
        vat: ['', [Validators.required, Validators.pattern('^(-?)(0|([1-9][0-9]*))(\\.[0-9]+)?$')]],
        taxNumber: ['', [Validators.required, Validators.pattern('^[0-9]*$'), Validators.maxLength(9), Validators.minLength(9)]],
        taxOffice: ['', Validators.required],
        headquarters: ['', null],

        cashBack: ['', null],
        cashbackPercentage: ['', null],
        ratioYummy: ['', null],
        ratioMerchant: ['', null],
        yummyCommission: ['', null],
        customerPercentage: ['', null],
        stamps: ['', null],
        iBankPay: ['', null]
        // Policies and schedule exist in merchant-data.component.html but are not currently used as features
        // policies: ['', null],
        // schedule: ['', null]
      }
    );

  }


  contextMenuEvent(e: any): void {
    this.selectedRowData = e;
  }

  async itemClick(e: any): Promise<void> {
    switch (e.itemIndex) {
      case 0:
        await this.sendNotification();
        break;
      case 1:
        await this.sendEmail();
        break;
    }
  }

  async sendNotification(): Promise<void> {
    const dialog = this.dialog.open(PushContentDialogComponent, {});
    dialog.afterClosed().subscribe(async (data: any) => {
      if (data) {
        try {
          const tokens = this.selectedRowData
            .filter((customer: any) => customer.fcmToken)
            .map((customer: any) => {
              if (
                customer.fcmToken !== null &&
                customer.fcmToken !== '' &&
                customer.fcmToken !== undefined
              ) {
                return customer.fcmToken;
              }
            });
          await this.adminService.sendNotification(
            tokens,
            data,
            this.merchantId
          );
          this.toastMessagesService.toastMessages('Οι ειδοποιήσεις στάλθηκαν με επιτυχία');
        } catch (e) {
          this.toastMessagesService.toastErrorMessage(e.message);
        } finally {
          this.loadingService.setLoading(false);
        }
        // Send Notifications
      }
    });
  }

  async sendEmail(): Promise<void> {
      const dialog = this.dialog.open(EmailCreatorDialogComponent, {});
      dialog.afterClosed().subscribe(async (data: any) => {
        if (data) {
          try {
            this.loadingService.setLoading(true);
            const emails = this.selectedRowData
              .filter((customer: any) => customer.identifier)
              .map((customer: any) => {
                if (
                  customer.identifier !== null &&
                  customer.identifier !== '' &&
                  customer.identifier !== undefined
                ) {
                  return customer.identifier;
                }
              });

            const body = {
              recipients: emails,
              storeId: this.merchantId,
              notification: {
                title: data.title,
                body: data.body
              }
            };
            await this.adminService.sendEmail(body);
            this.toastMessagesService.toastMessages('Η αποστολή των emails ολοκληρώθηκε με επιτυχία');
          } catch (e) {
            this.toastMessagesService.toastErrorMessage(e.message);
          } finally {
            this.loadingService.setLoading(false);
          }
        }
      });

  }
  /**
   * Fetches store data and image on init.
   */
  async ngOnInit(): Promise<void> {
    this.route.params.subscribe(async params => {
      this.merchantId = +params.id;

      if (!this.merchantId) {
        this.toastMessagesService.toastErrorMessage('Κάποιο σφάλμα προέκυψε');
        return;
      }

      try {
        this.loadingService.setLoading(true);
        // this.imageSource = await this.fetchStoreFrontImage(this.merchantId);
        // if (!this.imageSource) {
        //   this.imageSource = 'assets/images/store-default.png';
        // }
        await this.getMerchantData();

        this.form.setValue(this.merchantObject);
        this.registration = localStorage.getItem('registration') === 'true';
        // Depending on whether the user navigates stores or requests tab
        // if (this.registration) {
        //   this.getRegistrationData().then(() => this.form.setValue(this.merchantObject));
        // } else {
        //   this.getMerchantData().then(() => this.form.setValue(this.merchantObject));
        // }
      } catch (e) {
        this.toastMessagesService.toastErrorMessage(e.message);
      } finally {
        this.loadingImage = true;
        this.loadingService.setLoading(false);
      }
    });
  }

  populateCards(): void {
    const uniqueCustomers: any = [];
    this.cardInfo[0].value = this.data[0].bucket + ' €';

    const sum = this.orders.reduce((accumulator: any, currentValue: any) => {
      return accumulator + currentValue.price;
    }, 0);

    for (const order of this.orders) {
      if (!uniqueCustomers.find((c: any) => c.id === order.base_customer.id)) {
        uniqueCustomers.push(order.base_customer);
      }
    }

    this.cardInfo[1].value = sum.toFixed(2) + ' €';
    this.cardInfo[3].value = this.orders.length;
    this.cardInfo[2].value = uniqueCustomers.length;
    this.cardInfo[4].value = this.emails.length;
    this.cardInfo[5].value = this.notifications.length;
  }

  populateCharts(): void {
    const ordersPerDay = [];
    const groupedOrdersPerDay = this.commonService.groupBy(this.orders, 'orderDateOnly');
    const groupedCustomers = this.commonService.groupBy(this.orders, 'base_customer.id');

    // tslint:disable-next-line:forin
    for (const key in groupedOrdersPerDay) {
      const model = {
        label: key,
        value: groupedOrdersPerDay[key].length
      };
      ordersPerDay.push(model);
    }
    this.charts[0].data = ordersPerDay;

    const tempCustomers = [];
    // tslint:disable-next-line:forin
    for (const key in groupedCustomers) {
      let array = groupedCustomers[key];
      array = array.sort(
        (p1, p2) => (p1.orderDate < p2.orderDate) ? 1 : (p1.orderDate > p2.orderDate) ? -1 : 0);
      tempCustomers.push(array[0]);
    }

    tempCustomers.forEach(c => {
      this.customers.push(c.base_customer);
    });
    const groupedDates = this.commonService.groupBy(tempCustomers, 'orderDateOnly');
    const customers = [];
    // tslint:disable-next-line:forin
    for (const key in groupedDates) {
      const model = {
        label: key,
        value: groupedDates[key].length
      };
      customers.push(model);
    }
    this.charts[1].data = customers;

  }

  /**
   * Fetches store image.
   * If an image is not found, then a stock photo is displayed.
   */
  async fetchStoreFrontImage(storeId: number): Promise<string> {
    try {
      return await this.commonService.getRequest(`stores/frontImage/data/${storeId}`);
    } catch (e) {
      if (e.status === 404) {
        return '';
      }
      throw e;
    }
  }

  /**
   * Constructs the merchant object
   */
  async getMerchantData(): Promise<void> {
    try {
      this.data = await this.adminService.getMerchantById(+this.merchantId);
      this.orders = await this.adminService.getOrdersByMerchantId(+this.merchantId);
      for (const order of this.orders) {
        const tempDate = order.orderDate.split(' ');
        order.orderDateOnly = tempDate[0];
      }
      this.emails = await this.adminService.fetchEmailLogs(this.merchantId);
      this.notifications = await this.adminService.fetchNotificationLogs(this.merchantId);
      this.populateCards();
      this.populateCharts();

      this.merchantObject = {
        legalName: this.data[0].legalName,
        street: this.data[0].street,
        cityName: this.data[0].cityName,
        postalCode: this.data[0].postalCode,
        countryName: this.data[0].countryName,
        latitude: this.data[1].latitude,
        longitude: this.data[1].longitude,
        descriptionEl: this.data[0].descriptionEl,
        descriptionEn: this.data[0].descriptionEn,
        email: this.data[0].email,
        telephone: this.data[0].telephone,
        website: this.data[0].website,
        facebook: this.data[0].facebook,
        instagram: this.data[0].instagram,
        twitter: this.data[0].twitter,
        taxLegalName: this.data[0].taxLegalName,
        taxProfession: this.data[0].taxProfession,
        vat: this.data[0].vat,
        taxNumber: this.data[0].taxNumber,
        taxOffice: this.data[0].taxOffice,
        headquarters: this.data[0].headquarters,

        cashbackPercentage: 10,
        ratioYummy: 50,
        ratioMerchant: 50,
        yummyCommission: 5,
        customerPercentage: 5,

        cashBack: this.data[1].cashBack,
        stamps: this.data[1].stamps,
        iBankPay: this.data[1].iBankPay
      };
      this.storeName = this.data[0].legalName;
    } catch (e) {
      this.toastMessagesService.toastErrorMessage(e.message);
    } finally {
    }
  }

  /**
   * Used for the requests tab on dashboard
   * Currently hidden
   */
  async getRegistrationData(): Promise<void> {
    try {
      this.data = await this.adminService.getRegistrationById(localStorage.getItem('currentRegistration'));
    } catch (e) {
      this.toastMessagesService.toastErrorMessage(e.message);
    } finally {
    }
  }

  updateSlider(event: any): void {
    this.form.controls.ratioYummy.setValue(event.value);
    this.form.controls.ratioMerchant.setValue(100 - event.value);
    this.form.controls.yummyCommission.setValue(Math.round((((event.value * this.form.controls.cashbackPercentage.value) / 100) + Number.EPSILON) * 100) / 100);
    this.form.controls.customerPercentage.setValue(Math.round(((this.form.controls.cashbackPercentage.value - this.form.controls.yummyCommission.value)
      + Number.EPSILON) * 100) / 100);
  }


  /*
  Adding and removing store policies - if they are to be used requests to the server are needed
  addPolicy(): void {
    const dialogRef = this.dialog.open(AddPolicyDialogComponent, { data: { greekPolicy: '', englishPolicy: '' } });
    dialogRef.afterClosed().subscribe(async result => {
      if (result) {
        this.form.value.policies.push(result);
        this.table.renderRows();
      }
    });
  }

  deletePolicy(greekPolicy: any, englishPolicy: any): void {
    const dialogRef = this.dialog.open(DeletePolicyDialogComponent, { data: { greekPolicy, englishPolicy } });
    dialogRef.afterClosed().subscribe(async result => {
      if (result) {
        this.form.value.policies = this.form.value.policies.filter((item: { greekPolicy: any; englishPolicy: any; }) =>
          item.greekPolicy !== greekPolicy && item.englishPolicy !== englishPolicy);
        this.table.renderRows();
      }
    });
  }
  */

  /**
   * Updates store details
   */
  async updateMerchantData(): Promise<void> {
    try {
      const storeInfo = {id: this.merchantId};
      const storeOptions = {
        cashBack: this.form.controls.cashBack.value,
        stamps: this.form.controls.stamps.value,
        iBankPay: this.form.controls.iBankPay.value,
        customerPercentage: this.form.controls.customerPercentage.value,
        storeId: this.merchantId
      };
      await this.adminService.updateMerchantData(storeInfo, storeOptions, this.merchantId);
      this.toastMessagesService.toastMessages('Η ενημέρωση των στοιχείων ολοκληρώθηκε επιτυχώς!');
      await this.router.navigate(['dashboard/merchants']);
    } catch (e) {
      this.toastMessagesService.toastErrorMessage(e.message);
    } finally {
    }
  }


  /**
   *  Accepts the store's registration request to Yummy Wallet
   *  Currently not used
   */
  async declineRequest(): Promise<void> {
    try {
      await this.adminService.declineRegistrationRequest(this.data.id);
      await this.router.navigate(['dashboard/requests']);
      this.toastMessagesService.toastMessages('Η εγγραφή απορρίφθηκε επιτυχώς!');
    } catch (e) {
      this.toastMessagesService.toastErrorMessage(e.message);
    } finally {

    }
  }

  /**
   *  Declines the store's registration request to Yummy Wallet
   *  Currently not used
   */
  async acceptRequest(): Promise<void> {
    try {
      await this.adminService.acceptRegistrationRequest(this.data);
      await this.router.navigate(['dashboard/requests']);
      this.toastMessagesService.toastMessages('Η εγγραφή ολοκληρώθηκε!');
    } catch (e) {
      this.toastMessagesService.toastErrorMessage(e.message);
    } finally {

    }
  }

}
