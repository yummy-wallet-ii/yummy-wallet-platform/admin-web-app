import { AfterViewInit, Component, ViewChild } from '@angular/core';
import { RequestItem } from '../../models/RequestItem';
import { AdminService } from '../../services/admin.service';
import { ToastMessagesService } from '../../services/toast-messages.service';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';

@Component({
  selector: 'app-requests',
  templateUrl: './requests.component.html',
  styleUrls: ['./requests.component.scss']
})
export class RequestsComponent implements AfterViewInit {

  @ViewChild(MatPaginator) paginator!: MatPaginator;

  dataSource: any;
  cardSource: any;
  hasData: boolean;
  columns = [
    {
      columnDef: 'taxNumber',
      header: 'ΑΦΜ Επιχείρησης',
      cell: (element: RequestItem) => `${element.taxNumber}`,
    },
    {
      columnDef: 'legalName',
      header: 'Επωνυμία Επιχείρησης',
      cell: (element: RequestItem) => `${element.legalName}`,
    },  {
      columnDef: 'city',
      header: 'Πόλη',
      cell: (element: RequestItem) => `${element.cityName}`,
    },  {
      columnDef: 'address',
      header: 'Διεύθυνση',
      cell: (element: RequestItem) => `${element.headquarters}`,
    },  {
      columnDef: 'email',
      header: 'Email',
      cell: (element: RequestItem) => `${element.email}`,
    },  {
      columnDef: 'phoneNumber',
      header: 'Τηλέφωνο',
      cell: (element: RequestItem) => `${element.telephone}`,
    },  {
      columnDef: 'requestDate',
      header: 'Ημερομηνία Αιτήματος',
      cell: (element: RequestItem) => `${element.requestDate}`,
    }
  ];
  displayedColumns = this.columns.map(c => c.columnDef);

  constructor(private adminService: AdminService, public toastMessagesService: ToastMessagesService, public router: Router, ) {
    this.hasData = false;
  }

  async ngAfterViewInit(): Promise<void> {
    await this.getRequests();
  }

  /**
   * Fetches registration requests to Yummy Wallet
   */
  async getRequests(): Promise<any> {
    try {
      this.dataSource = new MatTableDataSource<RequestItem>(await this.adminService.getRequests()) ;
      this.hasData = this.dataSource.data.length > 0;
      this.cardSource = this.dataSource.data;
      console.log(this.cardSource);
      this.dataSource.paginator = this.paginator;
      this.dataSource.paginator._intl.itemsPerPageLabel = 'Αιτήματα ανά σελίδα';
    } catch (e) {
      this.toastMessagesService.toastErrorMessage(e.message);
    } finally {

    }
  }

  /**
   * Navigates to a store's registration form
   * @param id
   */
  async showRegistrationData(id: any): Promise<void> {
    try {
      localStorage.setItem('currentRegistration', id);
      localStorage.setItem('registration', 'true');
      await this.router.navigate(['dashboard/merchant/data']);
    } catch (e) {
      this.toastMessagesService.toastErrorMessage(e.message);
    } finally {

    }
  }

  /**
   * Searches the registration requests table
   * @param event
   */
  search(event: any): void{
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
}
