import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-graph-pie',
  templateUrl: './graph-pie.component.html',
  styleUrls: ['./graph-pie.component.scss']
})
export class GraphPieComponent implements OnInit{
  @Input() data: any[] = [];
  @Input() title = '';
  @Input() loading = true;

  constructor() {
  }

  ngOnInit(): void {
    console.log(this.data);
  }

  customizeTooltip(arg: any): any {
    return {
      text: `${arg.argumentText}: ${arg.valueText}`,
    };
  }

  customizeLabel(point: any): string {
    return `${point.argumentText}: ${point.valueText}`;
  }
}
