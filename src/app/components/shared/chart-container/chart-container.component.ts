import {Component, Input, OnInit} from '@angular/core';
import {ChartType} from '../../../models/ChartType';

@Component({
  selector: 'app-chart-container',
  templateUrl: './chart-container.component.html',
  styleUrls: ['./chart-container.component.scss']
})
export class ChartContainerComponent implements OnInit {
  @Input() title = '';
  @Input() data: any = null;
  @Input() type: ChartType | undefined;
  constructor() { }

  ngOnInit(): void {
  }

  isBar(): boolean { return this.type === ChartType.Bar; }

  isLine(): boolean { return this.type === ChartType.Line; }

  isPie(): boolean { return this.type === ChartType.Pie; }

}
