import {Component, OnInit, Input, OnChanges, SimpleChanges} from '@angular/core';

@Component({
  selector: 'app-loading',
  templateUrl: './loading.component.html',
  styleUrls: ['./loading.component.scss']
})
export class LoadingComponent implements OnInit {
  @Input() loading = false;
  height: number;
  constructor() {
    this.height = window.innerHeight - 60;
  }

  ngOnInit(): void {
  }
}
