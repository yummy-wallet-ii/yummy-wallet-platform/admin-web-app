import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ExtremeEditorComponent } from './extreme-editor.component';

describe('ExtremeEditorComponent', () => {
  let component: ExtremeEditorComponent;
  let fixture: ComponentFixture<ExtremeEditorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ExtremeEditorComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ExtremeEditorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
