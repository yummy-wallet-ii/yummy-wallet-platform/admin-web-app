import {Component, EventEmitter, OnInit, Output} from '@angular/core';
class TabConfig {
  name!: string;
  value!: string[];
}
const tabsData: TabConfig[] = [
  { name: 'From This Device', value: ['file'] },
  { name: 'From the Web', value: ['url'] },
  { name: 'Both', value: ['file', 'url'] },
];
@Component({
  selector: 'app-extreme-editor',
  templateUrl: './extreme-editor.component.html',
  styleUrls: ['./extreme-editor.component.scss']
})
export class ExtremeEditorComponent implements OnInit {
  @Output() htmlValueChanged: EventEmitter<any> = new EventEmitter<any>();
  tabs: TabConfig[];
  currentTab: string[];
  constructor() {
    this.tabs = tabsData;
    this.currentTab = this.tabs[2].value;
  }

  ngOnInit(): void {
  }

  handleValueChange($event: any): void {
    this.htmlValueChanged.emit($event.value);
  }
}
