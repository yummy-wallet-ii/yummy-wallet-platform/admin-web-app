import {Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';

@Component({
  selector: 'app-chart-bar',
  templateUrl: './chart-bar.component.html',
  styleUrls: ['./chart-bar.component.scss']
})
export class ChartBarComponent implements OnInit, OnChanges{
  @Input() data: any[] = [];
  @Input() title = '';
  @Input() loading = true;
  @Input() isDouble = false;
  @Input() isHorizontal = true;
  internalHeight: number;
  constructor() {
    this.internalHeight = 440;
  }

  ngOnInit(): void {
    this.internalHeight = this.data?.length > 10 ? 800 : 440;
    console.log(this.data);
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.internalHeight = this.data?.length > 10 ? 800 : 440;
  }

  customizeLabel(point: any): string {
    return `${point.argumentText}: ${point.valueText}`;
  }

  customizeTooltip(arg: any): any {
    return {
      text: `${arg.argumentText}: ${arg.valueText}`,
    };
  }

  customizeTooltipForDouble(arg: any): any {
    return {
      text: `${arg.seriesName}: ${arg.valueText}`,
    };
  }
}
