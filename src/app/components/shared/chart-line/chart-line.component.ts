import {Component, Input, OnChanges, OnInit, SimpleChanges, ViewChild} from '@angular/core';
import {DxChartComponent} from "devextreme-angular";

@Component({
  selector: 'app-chart-line',
  templateUrl: './chart-line.component.html',
  styleUrls: ['./chart-line.component.scss']
})
export class ChartLineComponent implements OnInit, OnChanges {
  @ViewChild('chart', {static: true}) chart: DxChartComponent | undefined;
  @Input() data: any[] = [];
  @Input() title = '';
  @Input() loading = true;
  constructor() {
  }

  ngOnInit(): void {
  }
  ngOnChanges(changes: SimpleChanges): void {
    try {
      if (this.chart) {
        this.chart.instance.render({
          force: true,
          animate: false
        });
      }
    } catch (e) {
    }
  }

}
