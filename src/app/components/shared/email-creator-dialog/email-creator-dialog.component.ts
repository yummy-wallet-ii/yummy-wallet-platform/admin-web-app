import { Component, OnInit } from '@angular/core';
import {Notification} from '../../../models/Notification';
import {MatDialogRef} from '@angular/material/dialog';

@Component({
  selector: 'app-email-creator-dialog',
  templateUrl: './email-creator-dialog.component.html',
  styleUrls: ['./email-creator-dialog.component.scss']
})
export class EmailCreatorDialogComponent implements OnInit {
  notification: Notification = {} as Notification;
  constructor(public dialogRef: MatDialogRef<EmailCreatorDialogComponent>) { }

  ngOnInit(): void {
  }

  cancelDialog(): void {
    this.close();
  }

  confirmDialog(): void {
    this.close(this.notification);
  }

  htmlValueChanged(data: any): void {
    this.notification.body = data;
  }

  close(data?: Notification): void {
    this.dialogRef.close(data);
  }

}
