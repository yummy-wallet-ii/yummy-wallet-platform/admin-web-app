import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EmailCreatorDialogComponent } from './email-creator-dialog.component';

describe('EmailCreatorDialogComponent', () => {
  let component: EmailCreatorDialogComponent;
  let fixture: ComponentFixture<EmailCreatorDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EmailCreatorDialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EmailCreatorDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
