import {Component, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
// @ts-ignore
import {DxDataGridComponent} from 'devextreme-angular';
import {TableColumn} from '../../../models/TableColumn';

@Component({
  selector: 'app-extreme-table',
  templateUrl: './extreme-table.component.html',
  styleUrls: ['./extreme-table.component.scss']
})
export class ExtremeTableComponent implements OnInit {
  @Input() columns: TableColumn[] = [];
  @Input() data: any[] = [];
  @Input() hasSelection: boolean = false;
  @Input() noDataText = '';
  @Input() hasActions = false;
  @Input() actions: any[] = [];
  @Output() rowDoubleClickEvent: EventEmitter<any> = new EventEmitter<any>();
  @Output() contextMenuEvent: EventEmitter<any> = new EventEmitter<any>();
  @Output() navigateEvent: EventEmitter<any> = new EventEmitter<any>();
  @Output() downloadEvent: EventEmitter<any> = new EventEmitter<any>();
  @Output() addEvent: EventEmitter<any> = new EventEmitter<any>();
  @ViewChild('table', {static: false}) dataGrid!: DxDataGridComponent;
  readonly allowedPageSizes = [5, 10, 'all'];
  currentFilter: any;
  showFilterRow: boolean;
  showHeaderFilter: boolean;
  applyFilterTypes: any;
  columnsLoaded: boolean;
  windowWidth: number;
  isLoading: boolean;
  viewName: string;
  isViewSaving: boolean;
  isViewSubmitting: boolean;
  views: any;
  searchEditorOptions: any;
  internalHeight: number;
  selectedRow: any;
  selectedRowKeys: any[] = [];
  checkBoxesMode = 'always';

  constructor() {
    this.isLoading = false;
    this.showFilterRow = true;
    this.showHeaderFilter = true;
    this.applyFilterTypes = [{
      key: 'auto',
      name: 'Immediately',
    }, {
      key: 'onClick',
      name: 'On Button Click',
    }];

    this.currentFilter = this.applyFilterTypes[0].key;
    this.orderHeaderFilter = this.orderHeaderFilter.bind(this);
    this.windowWidth = window.innerWidth - 272;
    this.columnsLoaded = false;
    this.viewName = '';
    this.isViewSaving = false;
    this.isViewSubmitting = false;
    this.views = {
      isDisplaying: false,
      isLoading: true,
      isDeleting: false,
      isUpdating: false,
      records: []
    };

    this.searchEditorOptions = {
      format: 'dd-MM-yyyy' // Specify the desired date format for search field
    };

    this.internalHeight = window.innerHeight - 272;
  }

  ngOnInit(): void {
  }

  //#region EventEmitters
  navigateTo(row: any): void {
    this.navigateEvent.emit(row.data);
  }
  create(row: any): void {
    this.addEvent.emit(row.data);
  }
  download(row: any): void {
    this.downloadEvent.emit(row.data);
  }
  //#endregion
  //#region Extreme Table Helpers
  getAlignment(column: TableColumn): string {
    return column.alignment ? column.alignment : 'left';
  }

  onExporting(e: any): void {
  }

  prepareContextMenu(e: any): void {
    e.preventDefault();
    if (this.hasSelection) {
      const data = this.dataGrid.selectedRowKeys.length > 0 ? this.dataGrid.instance.getSelectedRowsData() : [];
      this.contextMenuEvent.emit(data);

    } else {
      this.contextMenuEvent.emit(this.selectedRow);
    }
  }

  setSelectedRow(e: any): void {
    this.selectedRow = e.row.data;
  }

  rowDoubleClick(e: any): void {
    this.rowDoubleClickEvent.emit(e.data);
  }

  displayCaption(label: string | null): string {
    if (!label) {
      return '';
    }
    if (label.length > 40) {
      label = label.slice(0, 40) + '...';
    }

    return label;
  }

  orderHeaderFilter(data: { dataSource: { postProcess: (results: any) => any; }; }): any {
    data.dataSource.postProcess = (results) => {
      results.push({
        text: 'Weekends',
        value: 'weekends',
      });
      return results;
    };
  }

  renderNonDateColumns(): any[] {
    return this.columns.filter(column => column.type !== 'date');
  }

  renderDateColumns(): any[] {
    return this.columns.filter(column => column.type === 'date');
  }

  //#endregion
  hasThisAction(actionName: string): boolean {
    if (!this.hasActions) {return false;}
    return this.actions.some(action => action === actionName);
  }
}
