import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {LoginComponent} from './components/login/login.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { UsersComponent } from "./components/users/users.component";
import { MerchantsComponent } from "./components/merchants/merchants.component";
import { RequestsComponent } from "./components/requests/requests.component";
import { MerchantDataComponent } from "./components/merchant-data/merchant-data.component";
import {MainComponent} from "./components/dashboard/main/main.component";
import {InvoicesComponent} from "./components/dashboard/invoices/invoices.component";
import {StoresComponent} from "./components/dashboard/stores/stores.component";
import {UnregisteredStoresComponent} from "./components/dashboard/unregistered-stores/unregistered-stores.component";
import {OrdersComponent} from "./components/dashboard/orders/orders.component";
import {PaymentsComponent} from "./components/dashboard/payments/payments.component";
import {InvoiceInfoComponent} from "./components/dashboard/invoices/invoice-info/invoice-info.component";
import {EmailsComponent} from "./components/dashboard/emails/emails.component";
import {NotificationsComponent} from "./components/dashboard/notifications/notifications.component";
import {RedeemRequestsComponent} from "./components/dashboard/redeem-requests/redeem-requests.component";

const routes: Routes = [
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full'
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'dashboard',
    component: DashboardComponent,
    children: [
      {
        path: '',
        redirectTo: 'main',
        pathMatch: 'full'
      },
      {
        path: 'main',
        component: MainComponent
      },
      {
        path: 'users',
        component: UsersComponent
      },
      {
        path: 'merchants',
        component: StoresComponent,
      },
      {
        path: 'merchant/data/:id',
        component: MerchantDataComponent
      },
      {
        path: 'invoices/info/:id',
        component: InvoiceInfoComponent
      },
      {
        path: 'requests',
        component: UnregisteredStoresComponent
      },
      {
        path: 'invoices',
        component: InvoicesComponent
      },
      {
        path: 'emails',
        component: EmailsComponent
      },
      {
        path: 'orders',
        component: OrdersComponent
      },
      {
        path: 'payments',
        component: PaymentsComponent
      },
      {
        path: 'notifications',
        component: NotificationsComponent
      },
      {
        path: 'redeem-requests',
        component: RedeemRequestsComponent
      }
    ]
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
